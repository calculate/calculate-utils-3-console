# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0 #
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from calculate.lib.datavars import Variable
import sys
from os import path

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_console3', sys.modules[__name__])


class VariableClGuiData(Variable):
    """
    Variable store path to data files
    """
    value = '/var/calculate/server'


class VariableClGuiDatabase(Variable):
    """
    Variable store name files containing clients certificates
    """

    def get(self):
        return path.join(self.Get('cl_gui_data'), "client_certs/Database")


class VariableClGuiImagePath(Variable):
    """
    Variable store path to images
    """
    value = '/usr/share/calculate/themes'
