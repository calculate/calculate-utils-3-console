#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from importlib import reload

def console_main():
    import sys
    from .application.cl_client import main, StoppableThread
    from .application.function import _print

    reload(sys)
    # sys.setdefaultencoding("utf-8")
    import builtins as __builtin__
    from calculate.lib.cl_lang import setLocalTranslate

    _ = lambda x: x
    setLocalTranslate('calculate_console', sys.modules[__name__])

    __builtin__.__dict__['_print'] = _print
    wait_thread = StoppableThread()
    try:
        sys.exit(main(wait_thread))
    except KeyboardInterrupt:
        wait_thread.stop()
        red = '\033[31m * \033[0m'
        print('\n' + red + _('Manually interrupted'))
        sys.exit(1)
