# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

__version__ = "3.1.8"
__app__ = "Calculate Console"

from calculate.lib.datavars import DataVars


class DataVarsConsole(DataVars):
    """Variable class for installation"""

    def importConsole(self, **args):
        """
        Заполнить конфигурацию переменных, для десктопа
        """
        self.importVariables()
        self.importVariables('calculate.core.variables')
        self.importVariables('calculate.console.variables')
        self.defaultModule = "console"
